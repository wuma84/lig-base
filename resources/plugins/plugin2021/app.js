// # vendors
// import 'bootstrap'


// # js
import './assets/js/app.js'

// #scss
import './assets/css/app.scss'

// svg
function requireAll(r) {
  r.keys().forEach(r)
}
requireAll(require.context('./assets/svg/', true, /\.svg$/))
