# README #

local (by flywheel) 用にカスタマイズした制作環境です。  
現状、とりあえず動くことを考えて作っています。  
みなさんの意見を取り入れていこうと思ってますので、レビューいただければと。  

## require

* node v16.13.0
* local (by flywheel)

## How do I get set up?

* local (by flywheel)でローカルサイトを作成
* このリポジトリをクローン  
localとgitは空のフォルダじゃないと作れないので、別名のフォルダにクローンして、コピペで移してください。その後、sourcetreeなどでリンク先を変えれば適用できるはずです。

* `npm ci`
* local(by flywheel)でローカルサイトを立ち上げる

## Commands

* `npm run dev`  
立ち上げたローカルサーバーにプロキシで繋いで、ファイル変更を監視しビルド&リロードします。localhost, localhost:3000 が仕様可能です。

* `npm run prod`  
プロダクションビルド。jsとcssをminifyします。

## Features

* laravel mix ではなくオリジナルwebpack
* 全てresourcesフォルダ内で管理
* ファイルの出力先を自由に設定できる
* browsersyncのssl証明書を用意

## 今できること

* browsersync
* sass
* jsのバンドル
* images（コピーしてdistへ出力）
* phpコピーしてdistへ出力）
* dev mode, prod mode（minify）
* svg（LIG用のをあまり理解していないので要調整）
* clean（不要ファイルを削除してビルド）

## 追加予定

* scss リンター
* js リンター
* image 圧縮
* webp
