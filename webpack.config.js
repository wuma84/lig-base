const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const autoprefixer = require('autoprefixer')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const globImporter = require('node-sass-glob-importer')
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts')
const TerserPlugin = require('terser-webpack-plugin')

const domain = 'liglocal.dev'
const theme = 'theme2021'
const plugin = 'plugin2021'

const dist = './app/public/wp-content/'

const app = {
  entry: {
    'theme': `./resources/themes/${theme}/app.js`,
    'plugin': `./resources/plugins/${plugin}/app.js`
  },
  output: {
    filename: (pathData) => {
      return pathData.chunk.name === 'theme' ? `themes/${theme}/app.js` : `plugins/${plugin}/app.js`
    },
    path: path.resolve(__dirname, dist),
    publicPath: '/',
  },
  resolve: {
    extensions: [
      '.js',
      '.css',
      '.scss',
      '.html',
      '.php',
      '.ejs',
      '.png',
      '.jpg',
      '.gif',
      '.svg',
      '.ico'
    ],
  },
  target: ['web', 'es5'],
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                "@babel/preset-env",
              ],
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader'
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  [
                    'autoprefixer', {
                      grid: true,
                      flexbox: true,
                    }
                  ],
                ],
              },
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                importer: globImporter()
              }
            }
          },
        ]
      },
      {
        test: /\.(jpg|png|gif|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images/',
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          'svg-sprite-loader',
          'svg-transform-loader',
          'svgo-loader'
        ]
      }
    ]
  },
  plugins: [
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000,
      proxy: `https://${domain}`,
      https: {
        key: 'ssl/browsersync.key',
        cert: 'ssl/browsersync.crt',
      }
    }),
    new MiniCssExtractPlugin({
      filename: (pathData) => {
        return pathData.chunk.name === 'theme' ? `themes/${theme}/app.css` : `plugins/${plugin}/app.css`
      },
      chunkFilename: '[id].css',
    }),
    new RemoveEmptyScriptsPlugin(),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [
        `themes/${theme}`,
        `plugins/${plugin}`
      ],
    }),
    new CopyPlugin({
      patterns: [
        {
          from: 'resources',
          globOptions: {
            ignore: [
              '**/.DS_Store',
              '**/_*.*',
              '**/*.ejs',
              '**/*.scss',
              '**/*.js',
              '**/*.svg',
              '**/images/*'
            ]
          }
        },
        {
          from: 'resources/themes/**/images/**/*',
          to: `themes/${theme}/images/[name].[ext]`
        },
        {
          from: 'resources/plugins/**/images/**/*',
          to: `plugins/${plugin}/images/[name].[ext]`
        },
      ],
    }),
  ],
  optimization: { // production minify
    minimizer: [
      new CssMinimizerPlugin(),
      new TerserPlugin({
        terserOptions: {
          ecma: 6,
          compress: true,
          output: {
            comments: false,
            beautify: false
          }
        },
        extractComments: false
      })
    ]
  },
}
module.exports = app
